public enum ComponentType
{
    Empty,
    Pawn,
    Bishop,
    Tower,
    DoubleDamage,
    Freeze,
    Arrow
}