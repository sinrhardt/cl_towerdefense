using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class Projectile : MonoBehaviour
{
    private IObjectPool<Projectile> _projectilesPool;
    [SerializeField] private float mapEdges = 16.0f;

    private float _damage = 1.0f;
    private bool _canPierce = false;
    private bool _canSlow = false;
    private float _slowValue = 1.0f;

    public void SetStats(float damage, bool canPierce, bool canSlow, float slowValue)
    {
        _damage = damage;
        _canPierce = canPierce;
        _canSlow = canSlow;
        _slowValue = slowValue;
    }

    public void MultiplyDmg(float dmg)
    {
        _damage *= dmg;
    }

    public void SetPool(IObjectPool<Projectile> projectilesPool)
    {
        _projectilesPool = projectilesPool;
    }

    private void Release()
    {
        _projectilesPool.Release(this);
    }
    

    void Update()
    {
        transform.position += Time.deltaTime * 20f * transform.forward;

        if (transform.position.x > mapEdges || transform.position.x < -mapEdges || transform.position.z > mapEdges || transform.position.z < -mapEdges)
        {
            Release();
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            Enemy enemy = other.GetComponent<Enemy>();
            enemy.Damaged(_damage);

            if (_canSlow)
            {
                enemy.Slowed(_slowValue);
            }
            
            if (!_canPierce)
            {
                Release();
            }
        }
        
    }
}
