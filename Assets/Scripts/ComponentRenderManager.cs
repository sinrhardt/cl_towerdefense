using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComponentRenderManager : MonoBehaviour
{
    [SerializeField] private GameObject pawnPrefab;
    [SerializeField] private GameObject bishopPrefab;
    [SerializeField] private GameObject towerPrefab;
    [SerializeField] private GameObject freezePrefab;
    [SerializeField] private GameObject arrowPrefab;
    [SerializeField] private GameObject doubleDamagePrefab;

    public static ComponentRenderManager Instance;

    private void Awake()
    {
        Instance = this;
    }



    public GameObject GetComponentPrefab(ComponentType componentType)
    {
        switch (componentType)
        {
            case ComponentType.Pawn:
                return pawnPrefab;
            case ComponentType.Bishop:
                return bishopPrefab;
            case ComponentType.Tower:
                return towerPrefab;
            case ComponentType.Freeze:
                return freezePrefab;
            case ComponentType.Arrow:
                return arrowPrefab;
            case ComponentType.DoubleDamage:
                return doubleDamagePrefab;
            default:
                return pawnPrefab;
        }

    }
    
    
    
}
