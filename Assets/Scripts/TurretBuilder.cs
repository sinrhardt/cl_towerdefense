using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class TurretBuilder : MonoBehaviour
{
    [SerializeField] private LayerMask componentLayerMask;
    [SerializeField] private LayerMask turretSpotLayerMask;
    [SerializeField] private Camera mainCamera;

    private ComponentType _activeComponent = ComponentType.Empty;

    private Ray _raycast;
    private RaycastHit _raycastHit;
    
    private void Update()
    {
        MouseButtonDown();
    }

    private void MouseButtonDown()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _raycast = mainCamera.ScreenPointToRay(Input.mousePosition);
            TryChangeActiveComponent();
            TryAddComponentToTurret();
        }
    }

    private void TryChangeActiveComponent()
    {
        if (Physics.Raycast(_raycast, out _raycastHit, Single.PositiveInfinity, componentLayerMask))
        {
            BoxCollider collider = _raycastHit.transform.gameObject.GetComponent<BoxCollider>();
            transform.position = new Vector3(collider.center.x + _raycastHit.transform.position.x, transform.position.y, collider.center.z + _raycastHit.transform.position.z);

            Component component = _raycastHit.transform.gameObject.GetComponent<Component>();
            _activeComponent = component.ComponentType;
        }
    }

    private void TryAddComponentToTurret()
    {
        if (Physics.Raycast(_raycast, out _raycastHit, Single.PositiveInfinity, turretSpotLayerMask))
        {
            TurretSpot turretSpot = _raycastHit.transform.gameObject.GetComponent<TurretSpot>();
            Debug.Log(turretSpot.AttachComponent(_activeComponent));
        }
    }
    
    
    
}
