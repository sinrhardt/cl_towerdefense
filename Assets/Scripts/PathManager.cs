using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Pool;
using UnityEngine.Serialization;

public class PathManager : MonoBehaviour
{
    [SerializeField] private Transform[] path;
    [SerializeField] private int maxEnemiesAlive = 3;
    [SerializeField] private float spawnCooldown = 2.0f;
    
    [SerializeField] private Enemy enemyPrefab;
    
    private IObjectPool<Enemy> _enemiesPool;
    
    private int _activeEnemies = 0;
    private bool _canSpawn = true;

    public static PathManager Instance { get; private set; }
    
    private void Awake()
    {
        Instance = this;
        _enemiesPool = new ObjectPool<Enemy>(CreateEnemy, OnGetEnemy, OnReleaseEnemy, OnDestroyEnemy, true);
    }

    #region Pooling
    
    private Enemy CreateEnemy()
    {
        Enemy enemy = Instantiate(enemyPrefab, path[0].position, Quaternion.identity);
        enemy.SetPool(_enemiesPool);
        return enemy;
    }
    
    private void OnGetEnemy(Enemy obj)
    {

        obj.transform.position = path[0].position;
        obj.ResetStat();
        obj.SetNextTarget(path[1].position);
        _activeEnemies++;
        obj.gameObject.SetActive(true);
    }
    
    private void OnReleaseEnemy(Enemy obj)
    {
        obj.gameObject.SetActive(false);
        _activeEnemies--;
    }
    private void OnDestroyEnemy(Enemy obj)
    {
        Destroy(obj);
        _activeEnemies--;
    }

    #endregion
    void Start()
    {
        
    }

    void Update()
    {
        if (_activeEnemies < maxEnemiesAlive && _canSpawn)
        {
            StartCoroutine(SpawnEnemy());
        }
    }

    private IEnumerator SpawnEnemy()
    {
        _canSpawn = false;
        _enemiesPool.Get();
        
        yield return new WaitForSeconds(spawnCooldown);
        _canSpawn = true;
    }

    public Vector3 GetNextTarget(int index)
    {
        return path[index].position;
    }
    
    
    // private void OnDrawGizmos()
    // {
    //     for (int i = 0; i < path.Length - 1; i++)
    //     {
    //         Handles.DrawDottedLine(path[i].position, path[i+1].position, 5);
    //     }
    // }
}
