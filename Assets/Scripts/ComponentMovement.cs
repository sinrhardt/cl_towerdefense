using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComponentMovement : MonoBehaviour
{
    private Vector3 _initialPos;
    private void Start()
    {
        _initialPos = transform.position;
    }

    void Update()
    {

        transform.position = _initialPos + new Vector3(0 , Mathf.Sin(Time.time) * 0.5f, 0);
    }
}
