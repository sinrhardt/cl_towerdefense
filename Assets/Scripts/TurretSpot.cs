using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Pool;

public class TurretSpot : MonoBehaviour
{
    [Header("Pawn Settings")]
    [SerializeField] private float pawnCooldown = 0.1f;
    [SerializeField] private float pawnDamage = 1.0f; 
    [Header("Tower Settings")]
    [SerializeField] private float towerCooldown = 0.1f;
    [SerializeField] private float towerDamage = 1.0f; 
    [Header("Bishop Settings")]
    [SerializeField] private float bishopCooldown = 0.1f;
    [SerializeField] private float bishopDamage = 1.0f;

    [Header("Slow Settings")] 
    [SerializeField] private float slowMultiplier = 0.5f;
    
    [Header("Damage Settings")] 
    [SerializeField] private float damageMultiplier = 2.0f;
    
    
    
    [Header("Turret Spot Settings")]
    [SerializeField] private int maxSlots = 3;
    [SerializeField] private Transform[] componentPositions;
    [SerializeField] private Projectile projectilePrefab;
    [SerializeField] private LayerMask enemiesLayerMask;


    private ComponentRenderManager _componentRenderManager;
    private ComponentType[] _componentTypes;
    private int _index;
    private IObjectPool<Projectile> _projectilePool;

    private bool _projectilePierce = false;
    private bool _projectileSlows = false;
    private float _currentSlow = 1.0f;
    private float _currentDamage = 1.0f;

    private void Start()
    { 
        _componentRenderManager = ComponentRenderManager.Instance;
        _projectilePool = new ObjectPool<Projectile>(CreateProjectile, OnGetProjectile, OnReleaseProjectile, OnDestroyProjectile);

        _componentTypes = new ComponentType[maxSlots];
        _index = 0;
    }

    public bool AttachComponent(ComponentType componentType)
    {
        if (!(_index + 1 <= maxSlots && componentType != ComponentType.Empty))
        {
            return false;
        }

        if (IsPowerUp(componentType) && !ContainsTurret())
        {
            return false;
        }
        
        
        _componentTypes[_index] = componentType;
        AddComponent();
        _index++;


        
        
        return true;
    }

    private void AddComponent()
    {
        switch (_componentTypes[_index])
        {
            case ComponentType.Pawn:
                T_Pawn pawn = this.AddComponent<T_Pawn>();
                AddComponentRender(_componentTypes[_index]);
                pawn.SetStats(pawnDamage, pawnCooldown, enemiesLayerMask);
                pawn.SetPool(_projectilePool);
                break;
            case ComponentType.Bishop:
                T_Bishop bishop = this.AddComponent<T_Bishop>();
                AddComponentRender(_componentTypes[_index]);
                bishop.SetStats(bishopDamage, bishopCooldown, enemiesLayerMask);
                bishop.SetPool(_projectilePool);
                break;
            case ComponentType.Tower:
                T_Tower tower = this.AddComponent<T_Tower>();
                AddComponentRender(_componentTypes[_index]);
                tower.SetStats(towerDamage, towerCooldown, enemiesLayerMask);
                tower.SetPool(_projectilePool);
                break;
            case ComponentType.Arrow:
                AddComponentRender(_componentTypes[_index]);
                _projectilePierce = true;
                break;
            case ComponentType.Freeze:
                AddComponentRender(_componentTypes[_index]);
                _projectileSlows = true;
                _currentSlow *= slowMultiplier;
                break;
            case ComponentType.DoubleDamage:
                AddComponentRender(_componentTypes[_index]);
                _currentDamage *= damageMultiplier;
                break;
        }
    }

    private void AddComponentRender(ComponentType componentType)
    {
        Instantiate(_componentRenderManager.GetComponentPrefab(componentType), componentPositions[_index]);
    }

    private bool ContainsTurret()
    {
        foreach (var VARIABLE in _componentTypes)
        {
            if (VARIABLE == ComponentType.Bishop || VARIABLE == ComponentType.Pawn || VARIABLE == ComponentType.Tower)
            {
                return true;
            }
        }

        return false;
    }

    private bool IsPowerUp(ComponentType componentType)
    {
        if (componentType == ComponentType.Arrow || componentType == ComponentType.Freeze || componentType == ComponentType.DoubleDamage)
        {
            return true;
        }

        return false;
    }

    
    
    #region Pooling
        
    private Projectile CreateProjectile()
    {
        Projectile projectile = Instantiate(projectilePrefab, transform.position, Quaternion.identity);
        projectile.SetPool(_projectilePool);
        return projectile;
    }
    
    private void OnGetProjectile(Projectile obj)
    {
        obj.SetStats(_currentDamage, _projectilePierce, _projectileSlows, _currentSlow);
        obj.transform.position = transform.position;
        obj.gameObject.SetActive(true);
    }
    
    private void OnReleaseProjectile(Projectile obj)
    {
        obj.gameObject.SetActive(false);
    }

    private void OnDestroyProjectile(Projectile obj)
    {
        Destroy(obj);
    }

    #endregion
}
