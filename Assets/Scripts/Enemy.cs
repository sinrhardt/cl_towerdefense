using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float speed = 5.0f;
    [SerializeField] private float health = 5;

    private float _currentHealth;
    private float _currentSpeed;
    
    
    
    private int _index;
    private Vector3 _targetPosition;
    
    private IObjectPool<Enemy> _pawnsPool;

    public void SetPool(IObjectPool<Enemy> enemiesPool)
    {
        _pawnsPool = enemiesPool;
    }

    private PathManager _pathManager;
    
    void Start()
    {
        _pathManager = PathManager.Instance;
        
        ResetStat();
    }

    void Update()
    {
        Move();
    }

    private void Move()
    {

        transform.position = Vector3.MoveTowards(transform.position, _targetPosition, speed * Time.deltaTime);

        if (transform.position == _targetPosition)
        {
            _index++;
            _targetPosition = _pathManager.GetNextTarget(_index);
        }
    }
    
    public void Release()
    {
        _pawnsPool.Release(this);
    }
    
    public void ResetStat()
    {
        _index = 1;
        _currentHealth = health;
        _currentSpeed = speed;
    }

    public void Damaged(float dmg)
    {
        _currentHealth -= dmg;

        if (_currentHealth <= 0)
        {
            _pawnsPool.Release(this);
        }
    }

    public void Slowed(float slow)
    {
        _currentSpeed *= slow;
    }

    public void SetNextTarget(Vector3 nextPosition)
    {
        _targetPosition = nextPosition;
    }
    
}
