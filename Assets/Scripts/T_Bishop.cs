using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class T_Bishop : MonoBehaviour
{
    private IObjectPool<Projectile> _projectilesPool;
    private TurretSpot _turretSpot;
    private float _damage;
    private float _cooldown;
    private LayerMask _enemiesLayerMask;
    private bool _canShoot = true;

    public void SetTurretSpot(TurretSpot turretSpot)
    {
        _turretSpot = turretSpot;
    }
    
    public void SetPool(IObjectPool<Projectile> projectilesPool)
    {
        _projectilesPool = projectilesPool;
    }

    public void SetStats(float damage, float cooldown, LayerMask enemiesLayerMask)
    {
        _damage = damage;
        _cooldown = cooldown;
        _enemiesLayerMask = enemiesLayerMask;
    }

    private void Shooting()
    {
        Vector3[] directions = new Vector3[4];
        directions[0] = (transform.forward + transform.right).normalized;
        directions[1] = (transform.forward - transform.right).normalized;
        directions[2] = ( - transform.forward - transform.right).normalized;
        directions[3] = (- transform.forward + transform.right).normalized;

        for (int i = 0; i < 4; i++)
        {
            if (Physics.Raycast(transform.position + new Vector3(0,3,0), directions[i], Single.PositiveInfinity, _enemiesLayerMask))
            {
                Projectile temp = _projectilesPool.Get();
                temp.MultiplyDmg(_damage);
                temp.transform.position += new Vector3(0, 3, 0);
                temp.transform.forward = directions[i];
            }
        }

        StartCoroutine(ShootingCooldown());
    }

    private IEnumerator ShootingCooldown()
    {
        _canShoot = false;
        yield return new WaitForSeconds(_cooldown);
        _canShoot = true;
    }

    void Update()
    {
        if (_canShoot)
        {
            Shooting();
        }
    }
    
}
