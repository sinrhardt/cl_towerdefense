using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class King : MonoBehaviour
{
    [SerializeField] private int health = 100;
    [SerializeField] private TextMeshProUGUI _textMeshProUGUI;

    private void Start()
    {
        PrintText();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            health--;
            PrintText();

            if (health == 0)
            {
                _textMeshProUGUI.SetText("Unlucky");
                Time.timeScale = 0;
                Application.Quit();
            }
            
            other.GetComponent<Enemy>()?.Release();
            
        }
    }

    private void PrintText()
    {
        _textMeshProUGUI.SetText("HP: " + health);
    }
}
