using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Component : MonoBehaviour
{
    [SerializeField] private ComponentType componentType;

    public ComponentType ComponentType => componentType;
    
}
